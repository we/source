import os


def main_menu():
    os.system('clear')
    print('QEMU超级虚拟机')
    print('           -----更方便的qemu虚拟机')
    print('')
    print('  [1]创建x86虚拟机    [2]虚拟机管理器')
    print('')
    print('  [3]虚拟磁盘管理器   [4]其他选项')
    print('')
    print('  [5]安装所有依赖     [6]退出')


def create_vm():
    os.system('clear')
    print('QEMU超级虚拟机')
    print('           -----更方便的qemu虚拟机')
    print('')
    print('创建虚拟机')
    vm_name = input('请输入新虚拟机名称:')
    os.system(f'mkdir qemu/vm/{vm_name}')
    print('\n 配置虚拟机')
    os.system('ls qemu/disk/')
    vm_disk0 = input('请输入虚拟磁盘名称:')
    os.system(f'echo qemu/disk/{vm_disk0} > qemu/vm/{vm_name}/disk0')
    vm_ram0 = input('请输入运行内存(单位MB):')
    os.system(f'echo {vm_ram0} > qemu/vm/{vm_name}/ram')
    vm_core0 = input('请输入CPU核心数:')
    os.system(f'echo {vm_core0} > qemu/vm/{vm_name}/core')
    option0 = input('是否配置网卡(按0跳过 按1继续):')
    if int(option0) == 0:
        os.system(f'echo > qemu/vm/{vm_name}/net')
        os._exit(0)
    
    elif int(option0) == 1:
        os.system('clear')
        os.system('qemu-system-x86_64 -net nic,model=?')
        vm_net_card = input('请输入网卡名称:')
        os.system(f'echo {vm_net_card} > qemu/vm/{vm_name}/net')
        os._exit(0)

    else:
        print('不合法的输入选项')
        os._exit(0)


def manage_vm():
    os.system('clear')
    print('QEMU超级虚拟机')
    print('           -----更方便的qemu虚拟机')
    print('')
    print('已创建的虚拟机')
    os.system('ls qemu/vm/')
    vm_manage = input('请输入要管理的虚拟机名称:')
    with open(f"qemu/vm/{vm_manage}/disk0", "r", encoding='UTF-8')as disk0:
        vm_disk0 = disk0.read()
    with open(f"qemu/vm/{vm_manage}/ram", "r", encoding='UTF-8')as ram:
        vm_ram = ram.read()
    with open(f"qemu/vm/{vm_manage}/core", "r", encoding='UTF-8')as core:
        vm_core = core.read()
    with open(f"qemu/vm/{vm_manage}/net", "r", encoding='UTF-8')as net:
        vm_net = net.read()

    print(f'虚拟机名称:{vm_manage}')
    print('')
    print(f'\t 虚拟磁盘0:{vm_disk0}')
    print(f'\t 运行内存(单位MB):{vm_ram}')
    print(f'\t CPU核心:{vm_core}')
    print(f'\t 虚拟网卡:{vm_net}')
    print('\n \n [1]临时挂载iso并从iso启动 [2]启动虚拟机 \n [3]删除该虚拟机 [4]修改虚拟机配置')
    operate = input('请输入你的选项:')
    if int(operate) == 1:
        os.system('clear')
        os.system('ls qemu/iso')
        vm_iso = input('请输入iso镜像名称:')
        os.system(f'qemu-system-x86_64 -hda {vm_disk0} -smp {vm_core}  -m {vm_ram} -boot d -cdrom qemu/iso/{vm_iso} -vga vmware')
        os._exit(0)

    elif int(operate) == 2:
        if os.access(f"qemu/vm/{vm_manage}/net", os.F_OK):
            os.system(f'qemu-system-x86_64 -hda {vm_disk0} -smp {vm_core} --accel tcg,thread=multi -m {vm_ram} -netdev user,id=user.0 -device {vm_net},netdev=user.0 -vga vmware')
        else:
            os.system(f'qemu-system-x86_64 -hda {vm_disk0} -smp {vm_core} --accel tcg,thread=multi -m {vm_ram} -vga vmware')
            os._exit(0)

    elif int(operate) == 3:
        os.system(f'rm -rf qemu/vm/{vm_manage} && rm -r qemu/vm/{vm_manage}')
        print('虚拟机:{vm_manage}已删除')
        os._exit(0)

    elif int(operate) == 4:
        os.system('clear')
        print('[1]修改虚拟磁盘  [2]修改运行内存  [3]修改CPU核心  [4]修改/添加虚拟网卡')
        option1 = input('请输入你的选项:')
        if int(option1) == 1:
            os.system('ls qemu/disk/')
            vm_disk1 = input('请输入虚拟磁盘名称:')
            os.system(f'echo qemu/disk/{vm_disk1} > qemu/vm/{vm_manage}/disk0')
            os._exit(0)

        elif int(option1) == 2:
            vm_ram1 = input('请输入运行内存(单位MB):')
            os.system(f'echo {vm_ram1} > qemu/vm/{manage_vm}/ram')
            os._exit(0)

        elif int(option1) == 3:
            vm_core1 = input('请输入CPU核心数:')
            os.system(f'echo {vm_core1} > qemu/vm/{manage_vm}/core')
            os._exit(0)

        elif int(option1) == 4:
            os.system('qemu-system-x86_64 -net nic,model=?')
            vm_net_card0 = input('请输入网卡名称:')
            os.system(f'echo {vm_net_card0} > qemu/vm/{manage_vm}/net')
            os._exit(0)


def manage_disk():
    os.system('clear')
    print('QEMU超级虚拟机')
    print('           -----更方便的qemu虚拟机')
    print('')
    os.system('ls qemu/disk')
    print('[1]查看指定磁盘信息  [2]新建虚拟磁盘  [3]删除指定虚拟磁盘')
    option2 = input('请输入你的选择:')
    if int(option2) == 1:
        os.system('ls qemu/disk')
        disk_info = input('请输入磁盘名称:')
        os.system('qemu-img info qemu/disk/{disk_info}')

    elif int(option2) == 2:
        print('QEMU虚拟机支持的磁盘镜像: raw、host_device、qcow2、qcow、cow、vdi、vmdk、vpc、cloop')
        print('推荐常用镜像格式:qcow2 vdi vmdk raw')
        disk_sys = input('请输入磁盘格式:')
        new_disk = input('请输入新建磁盘大小(单位GB):')
        disk_name = input('请输入新建磁盘名称:')
        os.system(f'qemu-img create -f {disk_sys} qemu/disk/{disk_name}.{disk} {new_disk}G')
        os._exit(0)

    elif int(option2) == 3:
        del_disk = input('请输入要删除的磁盘名称:')
        os.system(f'rm -rf qemu/disk/{del_disk} && rm -f {del_disk}')
        print('磁盘镜像:{del_disk}删除完成')
        os._exit(0)


def other():
    os.system('clear')
    print('QEMU超级虚拟机')
    print('           -----更方便的qemu虚拟机')
    print('')
    print(f'本地版本:beta1.0 制作日期:01月02日21:40')
    print(' [1]更新脚本  [2]关于')
    xuanze2 = input('\n请选择要执行的操作:')
    if int(xuanze2) == 1:
        updeta()

    elif int(xuanze2) == 2:
        about()

    else:
        print('不合法的输入选项')
        os._exit(0)


def about():
    print('作者信息:')
    print('bilibili UID:649699256')
    print('')
    print('开发群(QQ):938094081   \n 脚本开源地址:https://framagit.org/we/source/-/blob/main/qemu.py')
    os._exit(0)


def updeta():
    print('开始更新')
    os.system('pkg install wget -y && rm -rf qemu.py && wget https://framagit.org/we/source/-/raw/main/qemu.py && python qemu.py')
    os._exit(0)


def install():
    input('按任意键开始安装')
    os.system('mkdir qemu/ && mkdir qemu/disk && mkdir qemu/iso && mkdir qemu/vm')
    os.system('pkg install wget proot x11-repo unstable-repo qemu-utils qemu-system-x86_64-headless vim qemu-system-i386-headless -y')
    print('安装完成')
    input('按任意键继续')
    os._exit(0)


if __name__ == "__main__":

    copyright = main_menu()

    option = input('\n请选择要执行的操作: ')
    if int(option) == 1:
        create_vm()

    elif int(option) == 2:
        manage_vm()

    elif int(option) == 3:
        manage_disk()

    elif int(option) == 4:
        other()

    elif int(option) == 5:
        install()

    elif int(option) == 6:
        os._exit(0)

    else:
        print('不合法的输入选项')

